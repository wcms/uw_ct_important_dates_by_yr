<?php

/**
 * @file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>
    <?php print $user_picture; ?>
    <div class="highlight"></div>
    <?php
      $path = trim(base_path(), '/');
      $uw_api_key = variable_get('uw_api_key');
      global $user;
      $denied_voting_roles = array('anonymous user', 'unvalidated user');
      if (is_array($user->roles)):
        if (count(array_intersect($user->roles, $denied_voting_roles)) == 0):
        // Display content to authenticated users.
        if ($pub_to_live == 0):
          $pub_to_live_msg = "Dates publishing to PREVIEW ONLY. If you have published this year's dates to live before, an old revision is live.";
        else:
          $pub_to_live_msg = "Dates publishing to LIVE and preview.";
        endif;

        $uw_api_path = 'https://' . variable_get('uw_api_host', 'api.uwaterloo.ca') . '/';

        print "<em><b>Your important dates are " . $pub_to_live_msg . "</b></em>";
        print "<p><b>Manage deployment to Open Data:</b></p>";
        print "<div class=deployButtons><ul><li>
                <a class='deploy' href='${uw_api_path}v2/ping/importantdates.json?type=importantdates&site=" .
               $path . "&nid=" . $node->nid . "&pub_to_live=" . 0 . "&debug_mode=1&key=" . $uw_api_key . "'>Deploy Preview</a></li>";
        print "<li><a class=deploy href='${uw_api_path}v2/ping/importantdates.json?type=importantdates&site=" .
               $path . "&nid=" . $node->nid . "&pub_to_live=" . 1 . "&debug_mode=1&key=" . $uw_api_key . "'>Deploy Live</a></li>";
        print "<li><a class=delete href='${uw_api_path}v2/ping/importantdates.json?type=importantdates&site=" .
               $path . "&nid=" . $node->nid . "&action=delete&preview_only=1&debug_mode=1&key=" . $uw_api_key . "'>Delete Preview</a></li>";
        print "<li><a class=delete href='${uw_api_path}v2/ping/importantdates.json?type=importantdates&site=" .
               $path . "&nid=" . $node->nid . "&action=delete&debug_mode=1&key=" . $uw_api_key . "'>Delete All</a></li>";

      else:
        // Do not display content otherwise.
      endif;
      endif;

      $path = trim(base_path(), '/');

      print "<li><a class=preview href='https://uwaterloo.ca/quest/reload/important-dates/" . $academic_year . "/" . REQUEST_TIME . "'>Clear Cache on Quest (old)</a></li>";
      print "<li><a class=preview href='https://uwaterloo.ca/registrar/reload/important-dates/" . $academic_year . "/" . REQUEST_TIME . "'>Clear Cache on Registrar</a></li>";
      print_r("<li><a class=preview href=/" . $path . "/" . uw_open_data_important_dates_get_url_prefix() . "important-dates/" . $academic_year . ">Preview Results</a></li></ul></div>");
    ?>
    <br>
    <?php if ($display_submitted): ?>
      <div class="submitted"><?php print $date; ?> — <?php print $name; ?></div>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>

      <?php
      if (!empty($content['service_links'])) {
        hide($content['service_links']);
      }

      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
      ?>
    </div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif; ?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>

  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']);

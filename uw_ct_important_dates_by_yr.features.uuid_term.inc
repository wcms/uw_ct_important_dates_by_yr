<?php

/**
 * @file
 * uw_ct_important_dates_by_yr.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_important_dates_by_yr_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => '2016-2017',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '213cfdd4-41b4-407d-8aca-8b5a6ad22fd6',
    'vocabulary_machine_name' => 'academic_year',
    'field_term_lock' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Winter',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '49f0da29-16c1-496b-ae62-35a8ae984915',
    'vocabulary_machine_name' => 'academic_terms',
    'field_term_lock' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => '2018-2019',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '517ed287-ff92-440b-b995-3b8cec46a11d',
    'vocabulary_machine_name' => 'academic_year',
    'field_term_lock' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Returning Waterloo students',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '5818f931-8b17-4343-92f8-bd3be72d407c',
    'vocabulary_machine_name' => 'academic_status',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Fall',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '717a032e-acf7-46d6-8d8a-51f9a52d9c4b',
    'vocabulary_machine_name' => 'academic_terms',
    'field_term_lock' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => '2015-2016',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'ab4d1e16-1d69-4934-92ff-715dc29c91e2',
    'vocabulary_machine_name' => 'academic_year',
    'field_term_lock' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'New Waterloo students',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'adefb801-6aee-44a6-89dc-6a7fd5ff07f5',
    'vocabulary_machine_name' => 'academic_status',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => '2017-2018',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'f311beae-811d-43a9-aa25-0a5d24a95b73',
    'vocabulary_machine_name' => 'academic_year',
    'field_term_lock' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Spring',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => 'f90629ac-581e-4ece-bce9-dc096822fb89',
    'vocabulary_machine_name' => 'academic_terms',
    'field_term_lock' => array(
      'und' => array(
        0 => array(
          'value' => 1,
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}

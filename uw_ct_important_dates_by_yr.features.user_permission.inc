<?php

/**
 * @file
 * uw_ct_important_dates_by_yr.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_important_dates_by_yr_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access resource comment'.
  $permissions['access resource comment'] = array(
    'name' => 'access resource comment',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource field_collection_item'.
  $permissions['access resource field_collection_item'] = array(
    'name' => 'access resource field_collection_item',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource file'.
  $permissions['access resource file'] = array(
    'name' => 'access resource file',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource i18n_translation'.
  $permissions['access resource i18n_translation'] = array(
    'name' => 'access resource i18n_translation',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource menu_link'.
  $permissions['access resource menu_link'] = array(
    'name' => 'access resource menu_link',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource node'.
  $permissions['access resource node'] = array(
    'name' => 'access resource node',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource rate_limit'.
  $permissions['access resource rate_limit'] = array(
    'name' => 'access resource rate_limit',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource redirect'.
  $permissions['access resource redirect'] = array(
    'name' => 'access resource redirect',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource taxonomy_term'.
  $permissions['access resource taxonomy_term'] = array(
    'name' => 'access resource taxonomy_term',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource taxonomy_vocabulary'.
  $permissions['access resource taxonomy_vocabulary'] = array(
    'name' => 'access resource taxonomy_vocabulary',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource user'.
  $permissions['access resource user'] = array(
    'name' => 'access resource user',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource workbench_moderation_transition'.
  $permissions['access resource workbench_moderation_transition'] = array(
    'name' => 'access resource workbench_moderation_transition',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'access resource wysiwyg_profile'.
  $permissions['access resource wysiwyg_profile'] = array(
    'name' => 'access resource wysiwyg_profile',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restws',
  );

  // Exported permission: 'administer restful'.
  $permissions['administer restful'] = array(
    'name' => 'administer restful',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'restful',
  );

  // Exported permission: 'create uw_ct_imp_dates_academic_yr content'.
  $permissions['create uw_ct_imp_dates_academic_yr content'] = array(
    'name' => 'create uw_ct_imp_dates_academic_yr content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_imp_dates_academic_yr content'.
  $permissions['delete any uw_ct_imp_dates_academic_yr content'] = array(
    'name' => 'delete any uw_ct_imp_dates_academic_yr content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_imp_dates_academic_yr content'.
  $permissions['delete own uw_ct_imp_dates_academic_yr content'] = array(
    'name' => 'delete own uw_ct_imp_dates_academic_yr content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_ct_imp_dates_academic_yr content'.
  $permissions['edit any uw_ct_imp_dates_academic_yr content'] = array(
    'name' => 'edit any uw_ct_imp_dates_academic_yr content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_imp_dates_academic_yr content'.
  $permissions['edit own uw_ct_imp_dates_academic_yr content'] = array(
    'name' => 'edit own uw_ct_imp_dates_academic_yr content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_ct_imp_dates_academic_yr revision log entry'.
  $permissions['enter uw_ct_imp_dates_academic_yr revision log entry'] = array(
    'name' => 'enter uw_ct_imp_dates_academic_yr revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_imp_dates_academic_yr authored by option'.
  $permissions['override uw_ct_imp_dates_academic_yr authored by option'] = array(
    'name' => 'override uw_ct_imp_dates_academic_yr authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_imp_dates_academic_yr authored on option'.
  $permissions['override uw_ct_imp_dates_academic_yr authored on option'] = array(
    'name' => 'override uw_ct_imp_dates_academic_yr authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_imp_dates_academic_yr promote to front page option'.
  $permissions['override uw_ct_imp_dates_academic_yr promote to front page option'] = array(
    'name' => 'override uw_ct_imp_dates_academic_yr promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_imp_dates_academic_yr published option'.
  $permissions['override uw_ct_imp_dates_academic_yr published option'] = array(
    'name' => 'override uw_ct_imp_dates_academic_yr published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_imp_dates_academic_yr revision option'.
  $permissions['override uw_ct_imp_dates_academic_yr revision option'] = array(
    'name' => 'override uw_ct_imp_dates_academic_yr revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_imp_dates_academic_yr sticky option'.
  $permissions['override uw_ct_imp_dates_academic_yr sticky option'] = array(
    'name' => 'override uw_ct_imp_dates_academic_yr sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_ct_imp_dates_academic_yr content'.
  $permissions['search uw_ct_imp_dates_academic_yr content'] = array(
    'name' => 'search uw_ct_imp_dates_academic_yr content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}

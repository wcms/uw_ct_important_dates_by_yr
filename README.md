# Release Notes for Important Dates v2 RC 1
* Date: Friday 18 December 2015
* Prepared:
 * for: The Registrar's Office
 * by: Nathan Vexler | Information Systems and Technology | Open Data Service Lead

## URLs that matter:
* https://wms-feeds.uwaterloo.ca/important-dates/content/important-dates-2016-2017
 * Canonical view of published data shows up here
 * Note: On wms-feeds. Page has more functions when editor is logged in

* https://wms-feeds.uwaterloo.ca/important-dates/undergraduate-students/important-dates/2016-2017
 * Preview of the "Classic" Important Dates view shows up
 * Note: On wms-feeds. This page is generated using a hidden Open Data url

* https://wms-dev.uwaterloo.ca/quest/undergraduate-students/important-dates/2016-2017
 *We will be publishing the "Classic" Important Dates view here until go-live
 * Note: On wms-dev. This page is generated using a hidden/protected Open Data url

* https://uwaterloo.ca/quest/undergraduate-students/important-dates/2016-2017
 * When ready we can have data published straight to live production
 * publishing to here would constitute the official go-ahead.
 * Note: On PRODUCTION. This page is generated using a live Open Data url.
 * Open Data consumers (e.g. https://uwportal.uwaterloo.ca ) get these dates

## The Work-flow:
 * Step 1: Create content for first time; fill out minimum information necessary and at least one date
  * Click on Login | Click on Shortcuts | Click on Important dates for one academic calendar year
  * Navigate to https://wms-feeds.uwaterloo.ca/important-dates/node/add/uw-ct-imp-dates-academic-yr
 * Step 2: Click "Save" and do NOT select check box which "when [...] selected, [displays] content [on] the main public-facing site."
  * Content gets published to preview mode only; canonical view of data seen on
   * https://wms-sand7.uwaterloo.ca/nvexler/content/important-dates-[academic-year]
   * e.g. https://wms-feeds.uwaterloo.ca/important-dates/content/important-dates-2016-2017
   * Preview seen by clicking on "Preview result *here*". Please note that content that is not "published" cannot be previewed. As such we have set the default mode to published. Please note that content that is published does not by default end up on the Target Production site. The  check box which "when [...] selected, [displays] content [on] the main public-facing site." has to be selected
   * *WARNING* Selecting check box which "when [...] selected, [displays] content [on] the main public-facing site." forces content to be published live
 * Step 3: View draft on the preview page: click on link on published page which says "Preview result here"
 * Step 4: Keep on adding dates by
  * clicking "Add" on the node published page (e.g. https://wms-feeds.uwaterloo.ca/important-dates/content/important-dates-2016-2017) (when logged on)
  * clicking on the "New Draft tab" (when logged on)
 * Step 5: After adding content, if content is not updated in preview,  "click on Manual deploy of Important Dates to preview site (notice this link only shows up when logged in)"
 * Step 6: Go live by:
  * Clicking on check box which "when [...] selected, [displays] content [on] the main public-facing site."
  * Clicking on link on canonical view of data page (e.g. Manual deploy of Important Dates to production site here

## Known Issues
1. On the canonical data page, we see that dates that span one day still say from-to. E.g. Christmas is listed as from "Dec 25 2015 to Dec 25 2015" instead of "Dec 25 2015 to Dec 25" on the published page (this issue does not occur).
2. Notices regarding whether content is "published to preview"/"published to preview and live" don't query information from Open Data regarding which version number is live or if it's older than what's shown on the "wms-feeds Important Dates canonical page" (e.g. https://wms-feeds.uwaterloo.ca/important-dates/content/important-dates-2016-2017). A future update will display a link to the revision (e.g. https://wms-feeds.uwaterloo.ca/important-dates/17/revisions/42/view)  in the moderate console (e.g. https://wms-feeds.uwaterloo.ca/important-dates/node/17/moderation)


### Must have on Open Data Side
* Add logic so any dates including and greater than 2016-2017 use ImportantDatesv2.php (any older dates use important dates v1)
* Add webhook that responds to
 * GET /v2/ping/terms.json?type=importantdates&site=nvexler&nid=17&pub_to_live=1&key=[wcms-key]
 * GET /v2/ping/terms.json?type=importantdates&site=nvexler&nid=17&pub_to_live=0&key=[wcms-key]
 * Note that the site ping will be coming from wms-feeds.uwaterloo.ca/important-dates (this might be an issue becuase we assuming everything comes from uwaterloo.ca/slug); I can attempt to send more info... perhaps I can get wms-feeds from an environment variable; we can hard code this for now just on important dates
* Edit the /public/v2/services/schema.sql so that we load this data
* Have the script take in
 * https://api.uwaterloo.ca/v2/ping/terms.json?type=importantdates&site=nvexler&nid=17&pub_to_live=1&debug_mode=1&key=[wcms-key]
 * https://api.uwaterloo.ca/v2/ping/terms.json?type=importantdates&site=nvexler&nid=17&pub_to_live=1&debug_mode=1&key=[wcms-key]
* Have the ability to query
 * https://opendata-dev.uwaterloo.ca/~ztseguin/api-stp/v2/terms/1159/importantdates.json&preview=1&key=[wcms-key] or (notice only wcms hash is white listed)
 * https://opendata-dev.uwaterloo.ca/~ztseguin/api-stp/v2/terms/1159/importantdates.json?key=[api-key]
* Create pull request
* Note we will were hard code it for now for wms-feeds targetting wms-feeds for preview and opendata-dev for preview for now but then we will need to be able to target more slugs like news and events


### Improvements on Open Data Side

### Must have on Drupal Side
* Create new branch for uwvaluelists which points to custom url

### Improvements on both Open Data and Drupal Sides
* Have the ability to change targets using &target=slug

Improvements for Drupal Side:
* Check Open Data to see if stuff is published. If it isn't, put a warning in set message and create a link ot rectify it
* Asap: get right files in a make file
  182  git push origin FDSU-861_fix-web-hooks-workbench_moderation
  886  git push origin ODID-21_patch-for-restws
  922  git push origin ODID-21_fix-restws-d7.37+
  968  git push origin ODID-48_fix-render-multi-aud-same-term
  976  git push origin ODID-36_debug-connect-to-od
* test node backup/migrate
* in uw_open_data_important_dates improve sorting
* in uw_open_data_important_dates show something different that says the calendar will be published on a date set in scheduling of data if path is entered that doesn't have data
* Use EFQ to generate data to then theme from field collections
 * http://drupal.stackexchange.com/questions/28212/how-to-extract-values-of-field-collection-using-entityfieldquery
 * https://www.drupal.org/node/1343708
* Theme field collections better: http://fourkitchens.com/blog/article/better-way-theme-field-collections
* Create conditional formatter for date field so that
 * Only one date shows up if the date happens all day
* Generecise the Preview result here link
* Get uw_value_lists to take in $uw_api_host with different environment variables on produciton and dev / pilots

Debug Tips:
https://www.drupal.org/project/drupalforfirebug

  drupal_set_message('<pre>' . print_r($array_or_object_var, TRUE) . '</pre>');

http://drupal.stackexchange.com/questions/30385/finding-variables-available-in-preprocess-node-or-node-tpl-php-from-cck-fields
https://www.appnovation.com/blog/theming-view-within-your-module
https://www.drupal.org/node/190815 - Core templates
https://www.drupal.org/node/1804206 - How to print Taxonomy term on page.tpl drupal7
https://www.drupal.org/node/129295 - Best way to display field values from a referenced node (CCK - Contemplate)
http://stackoverflow.com/questions/8074485/drupal-7-how-to-render-custom-field
https://www.drupal.org/project/devel_debug_log
http://drupal.stackexchange.com/questions/75168/preprocess-functions-and-template-files
http://stackoverflow.com/questions/2383865/how-do-i-use-theme-preprocessor-functions-for-my-own-templates
https://www.drupal.org/node/223430 - Setting up variables for use in a template (preprocess and process functions)
https://snugug.com/musings/override-theme-functions-drupal-7-module/
tail -n 5 -f /var/log/httpd/access_log

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~archived notes below
Must have on Open Data Side
1. On first save for new data run webhook

Must have on Drupal Side
1. Add all dates for 2015-2016
2. Add all dates for 2016-2017
3. Test speed to see if it works
4. Document all deficits so far and ask for prioritizations
5 Fix custom formatters as much as possible
 After meeting
  6. itemize and commit all modules to drupal.org Org on gitlab
  7. commit all code
  8. idea: add a new flag for TBD at top level; if selected; publish last thing that is published

To do
1. figure out how I can get a service account


Custom Formatter
* If TBD add to class -tbd which I can then style pastel orange
* Hide TBD flag from output
* If Visible
* If both audiences are there, show no audience
* wrap same term in a class container
* date formatter to do "pretty dates"
https://gist.github.com/jb510/23dc81a2228296322811#file-pretty-php-date-ranges-php
* create field formatter that if counts 2 items hide
* create field formatter which renders https://wms-sand7.uwaterloo.ca/nvexler/admin/structure/field-collections/field-single-important-date/display - Single instance currently set to Fieldset of field collection items to

Other info:
https://www.drupal.org/project/date_range_formatter
https://www.drupal.org/node/1398966
https://www.phase2technology.com/blog/play-with-display-altering-drupal-field-collections/
http://fourword.fourkitchens.com/article/responsive-tables-field-collections-and-field-formatters
http://realize.be/introducing-field-formatter-conditions-drupal
https://www.drupal.org/project/ffc
https://www.drupal.org/project/issues/ffc?categories=All
http://fourword.fourkitchens.com/article/better-way-theme-field-collections


Nice to haves:

* people can enter date with May 25 2015 instead of 2015-05-25: http://www.drupalcontrib.org/api/drupal/contributions!date!date.api.php/function/hook_date_combo_process_alter/7 so that
* Unselect date range by default
* Select All students by default
* better sorting on display page

Idea for displaying results before publish
* Use RESTWS to login as service account user then detect status and pull data https://www.drupal.org/node/1664352

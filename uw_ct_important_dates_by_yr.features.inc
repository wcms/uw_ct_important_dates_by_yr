<?php

/**
 * @file
 * uw_ct_important_dates_by_yr.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_important_dates_by_yr_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_important_dates_by_yr_node_info() {
  $items = array(
    'uw_ct_imp_dates_academic_yr' => array(
      'name' => t('Important dates for one academic calendar year'),
      'base' => 'node_content',
      'description' => t('This is for important dates for one academic calendar year.'),
      'has_title' => '1',
      'title_label' => t('Important dates [Beginning year - End year]'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
